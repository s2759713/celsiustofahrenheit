package nl.utwente.di.celsiusToFahrenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double degrees = converter.getFahrenheit("1");
        Assertions.assertEquals(33.8, degrees ,0.0 , "Fahrenheit value ");

    }
}
