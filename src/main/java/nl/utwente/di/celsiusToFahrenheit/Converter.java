package nl.utwente.di.celsiusToFahrenheit;

public class Converter {
    public double getFahrenheit(String isbn) {
        double celsius = Double.parseDouble(isbn);
        return ( (double ) 9/5) * celsius + 32;
    }

}
